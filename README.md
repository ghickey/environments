# Environments

Testing and working environments for both Omnibus and charts.


## Notes for Kubernetes Clusters

Kubernetes clusters of various configurations are located in the `k8s`
directory. Each configuration contains a directory for each of the cloud
environments where it can be deployed. Once one has changed to the directory
for the specific cloud environment, `terragrunt apply-all` can be executed
to build the cluster. **Note:** Each environment will need to have the
IAM credentials created/referred to in order for `terragrunt` to create
objects in the cloud environment.

## Omnibus Environments

Most of the Omnibus environments use Docker Compose to create the testing
environment.

## GCP IAM Roles

There needs to be a service account created with the following roles added
to it:

- Compute Instance Admin (v1)
- Kubernetes Engine Admin
- Service Account Admin
- Service Account User
- Storage Admin
- Compute Security Admin
- Compute Network Admin
- Security Admin

Once the IAM service account is created, the credentials need to be
downloaded as a JSON file and stored locally. Before running any
`terragrunt` command, one must set `GOOGLE_APPLICATION_CREDENTIALS`
to point the the JSON file.

## Remote state files

The remote state files are now using the GitLab Terraform integration for
storing the remote states. To properly use this, the following block needs
to be added to the `terragrunt.hcl` file.

```
remote_state {
  backend = "http"
  config = {
    address = "${get_env("TF_HTTP_ADDRESS_BASE")}-<module>"
    lock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-<module>/lock"
    unlock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-<module>/lock"
  }
}
```

This requires that `<module>` be replaced with a label that describes
the Terragrunt module that the `terragrunt.hcl` creates. In addition, the
`TF_HTTP_ADDRESS_BASE` needs to be defined in the environment. This is
usually done in the `direnv` file located at the top of the Terragrunt
modules. The general format for `TF_HTTP_ADDRESS_BASE` is:

    https://gitlab.com/api/v4/projects/<project_id>/terraform/state/<unique_label>

The `<project_id>` can be the numeric ID or the project path of the GitLab
project where to create the state files. The `<unique_label>` is a combination
of labels that make each state file unique. The recommended format for
`<unique_label>` is `<env_tech>_<env_type>_<env_name>`. For example:
"k8s_standard_cloud-native".
