
include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../Cluster"]
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    #required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]
  }

  source = "git::ssh://git@gitlab.com/ghickey/tf-k8s-external-dns.git?ref=v0.7.0"
  #source = "/Users/hickey/gitlab/terraform/tf-k8s-external-dns"
}

remote_state {
  backend = "http"
  config = {
    address = "${get_env("TF_HTTP_ADDRESS_BASE")}-dns"
    lock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-dns/lock"
    unlock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-dns/lock"
  }
}

inputs = {
  namespace = "kube-system"
  cluster_state_file = "${get_env("TF_HTTP_ADDRESS_BASE")}-cluster"
  dns_provider = "aws"
}
