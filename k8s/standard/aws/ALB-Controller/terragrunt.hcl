
include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../Cluster"]
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    #required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]
  }

  source = "git::ssh://git@gitlab.com/ghickey/tf-aws-alb.git?ref=v1.1.0"
  #source = "/Users/hickey/gitlab/terraform/tf-aws-alb"
}

remote_state {
  backend = "http"
  config = {
    address = "${get_env("TF_HTTP_ADDRESS_BASE")}-alb"
    lock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-alb/lock"
    unlock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-alb/lock"
  }
}

inputs = {
  namespace = "kube-system"
  cluster_state_file = "${get_env("TF_HTTP_ADDRESS_BASE")}-cluster"

}
