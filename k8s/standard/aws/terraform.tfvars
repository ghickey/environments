
################################################################################
# The variables below are different between dev, stage and prod.
################################################################################

environment_name = "standard"
region           = "us-west-2"
location         = "us-west-2a"

owner            = "ghickey_at_gitlab_com"

name             = "ghickey-test"
