# If changing any of these variables, do a search/replace across this entire
# repo and ensure that any called modules reflect the correct variable names.

################################################################################
# The variables below are the same between dev, stage and prod.
################################################################################

product_name = "ghickey-cluster"

env_repo = "ghickey__environments"
project  = "cloud-native-182609"
department = "distribution"


