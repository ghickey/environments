
include {
  path = find_in_parent_folders()
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    #required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]

  }

  # source = "/Users/hickey/gitlab/terraform/tf-gke-cluster"
  source = "git::ssh://git@gitlab.com/ghickey/tf-gke-cluster.git?ref=v2.2.1"
}

remote_state {
  backend = "http"
  config = {
    address = "${get_env("TF_HTTP_ADDRESS_BASE")}-cluster"
    lock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-cluster/lock"
    unlock_address = "${get_env("TF_HTTP_ADDRESS_BASE")}-cluster/lock"
  }
}
