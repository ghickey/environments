
terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    required_var_files = ["${get_parent_terragrunt_dir()}/terraform.tfvars"]
    optional_var_files = ["${get_parent_terragrunt_dir()}/../common.tfvars"]
  }
}

remote_state {
  backend = "gcs"

  config = {
    bucket = "ghickey-terraform"
    prefix = "cloud-native/${path_relative_to_include()}"
  }
}
