
################################################################################
# The variables below are different between dev, stage and prod.
################################################################################

environment_name = "standard"
region           = "us-central1"
location         = "us-central1-b"

owner            = "ghickey_at_gitlab_com"
state_bucket     = "ghickey-terraform"
