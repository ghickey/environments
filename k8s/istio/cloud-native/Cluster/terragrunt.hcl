
include {
  path = find_in_parent_folders()
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    #required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]
  
  }

  #source = "git::ssh://git@gitlab.com/ghickey/tf-gke-cluster.git?ref=v0.1.5"
  source = "/Users/hickey/gitlab/terraform/tf-gke-cluster"
}

inputs = {
  name         = "ghickey-test"
  k8s_version  = "1.14.8-gke.12"
  enable_istio = "false"
}
