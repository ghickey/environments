
include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../Cluster"]
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    #required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]
  
  }

  source = "git::ssh://git@gitlab.com/ghickey/tf-gke-nodepool.git?ref=v0.1.3"
}

inputs = {
  node_count   = 2
  node_type    = "n1-standard-4"
  oauth_scopes = [ "https://www.googleapis.com/auth/ndev.clouddns.readwrite" ]
}
