
include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../Nodepool"]
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    #required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]
  }

  source = "git::ssh://git@gitlab.com/ghickey/tf-k8s-helm.git?ref=v0.1.0"
}

inputs = {
  rbac_enabled = "true"
  tiller_version = "v2.14.2"
}
