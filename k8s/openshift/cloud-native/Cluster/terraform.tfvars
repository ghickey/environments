
driver = "google"

node_count      = 3
name            = "node"
nugget          = "ghickey-openshift"

boot_disk_image = "rhel-8"
boot_disk_size  = "20"
instance_type   = "e2-standard-4"
preemptible     = "true"
scopes = [ "https://www.googleapis.com/auth/ndev.clouddns.readwrite" ]
post_exec_filename = "openshift-install.sh"

post_exec_
metadata = {
    enable-oslogin = "TRUE"
}

