#!/bin/bash

OPENSHIFT_INSTALL_URL='https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux.tar.gz'

cd /tmp
curl -o openshift-install-linux.tar.gz "$OPENSHIFT_INSTALL_URL"
gunzip openshift-install-linux.tar.gz
tar xf openshift-install-linux.tar.gz


cat <EOF >install-config.yaml
apiVersion: v1
baseDomain: example.com
compute:
  - hyperthreading: Enabled
    name: worker
    replicas: 0
controlPlane:
  hyperthreading: Enabled
  name: master
  replicas: 1
networking:
  networkType: OpenShiftSDN
  clusterNetwork:
    - cidr:
      hostPrefix:
  serviceNetwork:
    - CIDR
  machineCIDR: 10.0.0.0/16

metadata:
  name: test-cluster
platform:
  none: {}
pullSecret: '${REDHAT_PULL_SECRET}'
sshKey: '${SSH_PUB_KEY}'
EOF

./openshift-install create cluster --dir . --log-level info
