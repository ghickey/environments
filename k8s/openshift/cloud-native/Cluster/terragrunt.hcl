
include {
  path = find_in_parent_folders()
}

terraform {
  extra_arguments "root_tfvars" {
    commands = get_terraform_commands_that_need_vars()
    required_var_files = ["${get_terragrunt_dir()}/terraform.tfvars"]

  }

  #source = "git::ssh://git@gitlab.com/ghickey/tf-vm-cluster.git?ref=master"
  source = "../../../../../terraform//tf-vm-cluster"
}


