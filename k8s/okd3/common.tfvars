# If changing any of these variables, do a search/replace across this entire
# repo and ensure that any called modules reflect the correct variable names.

################################################################################
# The variables below are the same between dev, stage and prod.
################################################################################

product_name = "ghickey-okd"

env_repo = "ghickey__environments"
project  = "cloud-native-182609"
department = "distribution"

driver = "google"

# remote state locations 
# These locations need to have the cluster moniker pre-pended

# Seed_rs_key  = "cloud-native/Seed"

