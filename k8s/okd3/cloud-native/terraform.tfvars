
################################################################################
# The variables below are different between dev, stage and prod.
################################################################################

environment_name = "okd"
region           = "us-central1"
zone             = "us-central1-c"

owner            = "ghickey_at_gitlab_com"
state_bucket     = "ghickey-terraform"
