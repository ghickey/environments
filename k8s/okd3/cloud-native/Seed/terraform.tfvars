
driver  = aws

name            = "ghickey-okd-seed"
boot_disk_image = "centos-8"
instance_type   = "n1-standard-4"
preemptible     = "true"
service_account_scopes = []

post_exec_filename = "okd-install.sh"

metadata = {
    enable-oslogin = "TRUE"
}