Redis setups
============

This directory contains the setups for various Redis configurations and
the GitLab chart `values.yaml` files for using the configurations.

Required charts
---------------

| Chart            | Version           | Requirements |
|:-----------------|:------------------|:-------------|
| bitnami/redis    | 5.0.5             | >= 5.0.0     |
| gitlab/gitlab    |                   | >= 3.3.0     |

Single Redis server
-------------------

Can be used to demonstrate external Redis nodes. Can be created with

    helm upgrade --install <release> bitnami/redis -f single-node.yaml

Sentinel Redis server
---------------------

Use to bring up a 3 node Redis cluster with Sentinel support. Sentinels
run on port 26379 and configure the cluster name to be `mymaster`.

    helm upgrade --install <release> bitnami/redis -f ha-sentinel.yaml

If no Sentinel support is desired, the Redis cluster can be accessed
normally on port 6379.

Spinning up multiple Redis servers
----------------------------------

Using the `multi-redis.sh` script, it will spin up 4 Redis instances for
the `cache`, `queues`, `shared_state` and `actioncable` stores. Each are
placed in their own namespace (redis-<svc>). This allows the GitLab chart
to access each independent Redis instance as:

    redis-<svc>-master.redis-<svc>.svc

The Redis instances can be torn down again by running the script again
with the `-d` option. Running the script multiple times will just perform
and deployment upgrade.

Installing GitLab chart
-----------------------

The GitLab chart should be installed with the standard `values.yaml` file
and tack on the `gitlab-multi-redis.yaml` file.

    helm upgrade --install <release> gitlab/gitlab -f ../standard/gitlab.yaml -f gitlab-multi-redis.yaml

