#!/bin/bash

print_help() {
    echo "Usage: $0 [-d]"
    echo "    -d option will delete the redis deployments"
}

options=$(getopt dh "$@")
[ $? -eq 0 ] || {
    print_help
}
eval set -- "$options"

while [[ $# -gt "1" ]]; do
    case "$1" in
        -d)
            delete=1
            ;;
        -h)
            print_help
            ;;
        --)
            shift;
            ;;
    esac
    shift
done

for svc in 'cache' 'queues' 'shared-state' 'actioncable'; do
    if [[ "$delete" -eq 1 ]]; then
        echo "Deleting $svc Redis instance"
        helm uninstall redis-$svc -n redis-$svc
        kubectl delete ns redis-$svc
    else
        echo "Installing/upgrading $svc Redis instance"
        kubectl create ns redis-$svc
        helm upgrade --install redis-$svc bitnami/redis -n redis-$svc -f single-node.yaml
    fi
done
#helm upgrade --install

