chaoskube
=========

Reimplementation of chaos-monkey for Kubernetes

Setup of chaoskube
------------------

Tuneables:

| Name           | Default | Notes                                    |
|----------------|---------|------------------------------------------|
| interval       | 2m      | How often to kill a pod                  |
| namespace      | !kube-system | Namespaces to consider for terminations  |
| labels         |         | Labels for selection of pods             |

```console
helm upgrade --install chaoskube stable/chaostube -f chaoskube.yaml
```
