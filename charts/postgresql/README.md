PostgresQL setups
=================

This directory contains the setups for various PostgresQL configurations and
the GitLab chart `values.yaml` files for using the configurations.

It is expected that the PostgresQL pods are deployed to the `postgres` namespace.

Also note that the PostgresQL YAML files here create a `gitlab` user and
the `gitlabhq_production` database with the expectation that the deployed
instance will be used to support a GitLab installation.

Required charts
---------------

| Chart               | Version           | Requirements |
|:--------------------|:------------------|:-------------|
| bitnami/postgresql  | 8.10.5            | >= 8.0.0     |
| gitlab/gitlab       |                   | >= 3.3.0     |

Single PostgresQL database
--------------------------

Can be used to demonstrate external PostgresQL nodes. Can be created with

    helm upgrade --install <release> bitnami/postgresql -f postgresql.yaml
        --set postgresqlPostgresPassword=<PW> --set postgresqlPassword=<PW>

Clustered PostgresQL database
-----------------------------

Use to bring up a 3 node PostgresQL cluster.

    helm upgrade --install <release> bitnami/postgresql.yaml -f postgresql.yaml -f postgresql-cluster.yaml
        --set postgresqlPostgresPassword=<PW> --set postgresqlPassword=<PW>

Retrieving PostgresQL password
------------------------------

After deploying the PostgresQL chart, one needs to retrieve the postgresql
user password with the following command if it was allowed to be set randomly:

    kubectl get secret --namespace pg pg-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode

This will allow connecting to the database as the admin user.

    kubectl run pg-postgresql-client --rm --tty -i --restart='Never' --namespace postgres --image docker.io/bitnami/postgresql:11.8.0-debian-10-r19 --env="PGPASSWORD=<PW>" --command -- psql --host pg-postgresql -U postgres -d postgres -p 5432

Setting the PostgresQL password for GitLab
------------------------------------------

In most cases this will need to be used to create a secret for GitLab to be
able to connect to the PostgresQL instance/cluster.

    kubectl create secret -n gitlab generic gitlab-postgres --from-literal psql-password=<PASSWORD>

Installing GitLab chart
-----------------------

The GitLab chart should be installed with the standard `values.yaml` file
and tack on the `gitlab-multi-redis.yaml` file.

    helm upgrade --install <release> gitlab/gitlab -f ../standard/gitlab.yaml -f gitlab-multi-redis.yaml -n gitlab

