GitLab chart standard install
=============================

The files contained here are for standard installs of the GitLab chart.
The base file for the install is `gitlab.yaml` and should work for both
GKE and EKS. The domain name may need to be updated depending upon which
environment is being used.

All the additional files are used to turn on specific components of the
chart and are detailed below.

When deploying the chart, it is advisable that the `gitlab.yaml` file be
specified first and then any of the additional files be added afterwards.

    helm upgrade --install <REL> gitlab/gitlab -f gitlab.yaml -f <other.yaml>

outgoing-mail.yaml
------------------

This sets up the outgoing email to be sent through gmail.com. In order
for the outgoing email to be sent through Gmail, an application password
needs to be generated (assuming 2 factor security is turned on for the
account) by visiting the following URL:

    https://myaccount.google.com/apppasswords

The generated password then needs to be wrapped up in a Kubernetes
Secret:

    kubectl create secret generic outgoingmail-secret --from-literal=password=<app_password>

The `app_password` gets created without any spaces event though Google
presents it to the user with spaces.

servicedesk.yaml
----------------

Service Desk and incoming email are setup to use 2 mailboxes (`incomingemail`
and `servicedesk`) on a single account. Not the recommended setup, but it will
work. Note that any received email generally needs to be moved to the
appropriate folder and insured that the message shows up as unread in the
mailbox. Read messages can cause the processing component to throw an
exception when it sees a message that it did not process show up that is
not new.

Both components (incoming email and Service Desk) need to have an application
password set for Gmail. Generate the password at the following URL (the same
password used for outgoing mail can be used):

    https://myaccount.google.com/apppasswords

The generated password then needs to be wrapped up in a Kubernetes
Secret:

    kubectl create secret generic incomingmail-secret --from-literal=password=<app_password>
