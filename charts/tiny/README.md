Tiny Install
============
The tiny install is used for quick testing and does not provide any
redundancy or much in the way of features.

Things that have been removed from standard install:

- Prometheus
- Grafana
- Registry
