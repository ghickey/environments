Omnibus GitLab with External DB
===============================
The enclosed `docker-compose.yaml` file will create 3 containers for testing
Omnibus with an external DB. 

* gitlab-ce
* postgresql
* redis

To flush data from any previous installation one should remove the following
two directories prior to using `docker-compose`.

* `gitlab/opt`
* `postgres/data`

On a fresh install the database should not be created. Doing so has problems
where some of the components connect to the database and then one can not
seed the database and run the migrations.

_Note:_ `docker-compose` sometimes has some race conditions and the docker
network gets created for each container. It may be necessary to create the
docker network prior to running `docker-compose`. This is done with the
command `docker network create external-dbs_gitlab`.

Once the containers are running, one should exec into the `gitlab` container
and execute the following commands:

    # su - git
    $ gitlab-rake gitlab:setup
    $ ^D
    # gitlab-rake db:migrate:status
    # gitlab-ctl reconfigure

Once migrations have completed successfully, one should be able to connect
to http://localhost:30080/, set the root password and be able to log in as
the root user.
